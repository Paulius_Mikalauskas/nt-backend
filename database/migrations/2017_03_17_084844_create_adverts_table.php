<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adverts', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('type')->index();
            $table->string('city')->index()->nullable();
            $table->string('catchment')->index()->nullable();
            $table->string('street')->index()->nullable();
            $table->integer('area')->index()->nullable();
            $table->integer('story')->index()->nullable();
            $table->integer('stories')->index()->nullable();
            $table->integer('rooms')->index()->nullable();
            $table->integer('price')->index()->nullable();
            $table->string('description')->index()->nullable();
            $table->integer("user_id")->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adverts');
    }
}
