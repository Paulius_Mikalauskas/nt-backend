<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function(Blueprint $table) {
           $table->increments('id')->index();
           $table->string('url')->index();
           $table->string('thumbnail')->index();
           $table->integer('advert_id')->unsigned()->index();
           $table->foreign('advert_id')->references('id')->unsigned()->index()->on('adverts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("image");
    }
}
