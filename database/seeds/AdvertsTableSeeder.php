<?php

use Illuminate\Database\Seeder;

class AdvertsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $types = ["Butas", "Namas", "Kotedžas", "Sklypas", "Sodas", "Patalpa"];

        $adverts = [];

        for ($i = 0; $i < 30; $i++) {
            $advert = [
                'type' => $types[rand(0, 4)],
                "city" =>  $faker->city,
                "catchment" => $faker->streetName,
                "street" => $faker->address,
                "area" => rand(5, 100),
                "story" =>  rand(1, 15),
                "stories" => rand(1, 15),
                "rooms" => rand(1, 15),
                "price" =>rand(100, 10000),
                "description" => $faker->sentence($nbWords = 10, $variableNbWords = true),
                "user_id" => rand(1, 30)
            ];
            array_push($adverts, $advert);
        }
        DB::table('adverts')->insert($adverts);
    }
}
