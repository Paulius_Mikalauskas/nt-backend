<?php

use Illuminate\Database\Seeder;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $images = [];

        for($i = 1; $i <= 30; $i++) {
            for($j = 0; $j < rand(1, 6); $j++) {
                $url = $faker->imageUrl($width = 640, $height = 480, 'city');
                $image = [
                    'advert_id' => $i,
                    "url" => $url,
                    "thumbnail" => $url,
                ];
                array_push($images, $image);
            }
        }
        DB::table('images')->insert($images);
    }
}
