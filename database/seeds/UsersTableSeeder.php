<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $users = [];

        for($i = 0; $i < 10; $i++) {
            $user = [
                'name' => $faker->firstName,
                'email' => $faker->email,
                'password' => $faker->password,
                'surname' => $faker->lastName,
            ];
            array_push($users, $user);
        }

        DB::table('users')->insert($users);
    }
}
