<?php

namespace App\Http\Middleware;

use Closure;

use Auth0\SDK\JWTVerifier;
use Tymon\JWTAuth\Facades\JWTAuth;

class checkToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        \Log::info($request->auth);
        \Log::info($request->header('authorization'));
/*        $verifier = new JWTVerifier([
            'supported_algs' => ['RS256'],
            'valid_audiences' => [env('AUTH0_CLIENT')],
            'authorized_iss' => [env('AUTH0_ISS')],
        ]);*/
        // $verifier->verifyAndDecode($request->header('authorization'));
        return $next($request);
    }
}
