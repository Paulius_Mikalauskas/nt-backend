<?php
/**
 * Created by PhpStorm.
 * User: Paulius
 * Date: 5/12/2017
 * Time: 9:30 AM
 */

namespace App\Http\Controllers;
use App\User;
use Carbon\Carbon;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller {

    public function userValidation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|max:50|min:6'
        ]);

        if ($validator->fails())
        {
            abort(422);
        }
    }

    public function login(Request $request) {
        $this->userValidation($request);
        $key = "asd";
        $pass = User::where("email", $request->email)->pluck('password');
        Log::info($pass[1]);
        if(!Hash::check($request->password, $pass[1])) {
            abort(401);
        }
        $token = array(
            "iss" => "http://example.org",
            "aud" => "http://example.com",
            "iat" => Carbon::now(),
            "exp" => Carbon::now()->addWeeks(2)->timestamp,
            "nbf" => Carbon::now()->addSeconds(10),
            "isAdmin" => false,
        );
        $jwt = JWT::encode($token, $key);
        return Response::json($jwt)->header('Content-Type', 'application/json');
    }

    public function register(Request $request) {
        $this->userValidation($request);
        $password = Hash::make($request->password);
        $user = new User([
            'email' => $request->email,
            'password' => $password
        ]);
        $user->save();
    }
}