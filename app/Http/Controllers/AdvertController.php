<?php
/**
 * Created by PhpStorm.
 * User: Paulius
 * Date: 3/24/2017
 * Time: 11:33 AM
 */



namespace App\Http\Controllers;

use App;
use App\Advert;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AdvertController extends Controller
{
    public function advertValidation(Request $request) {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string|max:30',
            'city' => 'required|string|max:50',
            'catchment' => 'string|nullable|max:30',
            'street' => 'required|string|max:50',
            'area' => 'numeric|nullable',
            'story' => 'numeric|nullable',
            'stories' => 'numeric|nullable',
            'price' => 'numeric|nullable',
            'rooms' => 'numeric|nullable',
            'description' => 'string|nullable|max:2000'
        ]);

        if ($validator->fails())
        {
            abort(422);
        }
    }

    public function insert(Request $request)
    {
        $this->advertValidation($request);

        $advert = new Advert($request->only([
            'type', 'city', 'catchment', 'street',
            'area', 'story', 'stories', 'rooms',
            'price', 'description'
        ]));

        $advert->save();
        $advertId = $advert->id;
        $files = $request->file('files');
        $images = [];

        if($files == null) {
            $image = [
                "url" => env('BASE_CLOUDFRONT_URL') . 'default_home.jpg',
                "advert_id" => $advertId,
                "thumbnail" => env('BASE_CLOUDFRONT_URL') . 'default_homethumb.jpg'
            ];
            array_push($images, $image);
        } else {

            foreach ($files as $file) {
                $id = uniqid();
                $fileName = $id . '.jpeg';
                $fileThumbName = $id . 'thumb.jpeg';
                $this->fileUpload($file, $fileName, $fileThumbName);
                $url = env('BASE_CLOUDFRONT_URL') . $fileName;
                $image = [
                    "url" => $url,
                    "advert_id" => $advertId,
                    "thumbnail" => env('BASE_CLOUDFRONT_URL') . $fileThumbName
                ];
                array_push($images, $image);
            }
        }
        Image::insert($images);

        return $request->all();
    }

    public function update(Request $request, $advertId)
    {
        $this->advertValidation($request);

        Advert::where('id', $advertId)->update($request->only([
            'type', 'city', 'catchment', 'street',
            'area', 'story', 'stories', 'rooms',
            'price', 'description'
        ]));

        return $request->all();
    }

    public function addImage(Request $request, $advertId) {
        $file = $request->file('files')[0];
        $id = uniqid();
        $fileName = $id . '.jpeg';
        $fileThumbName = $id . 'thumb.jpeg';
        $this->fileUpload($file, $fileName, $fileThumbName);
        $url = env('BASE_CLOUDFRONT_URL') . $fileName;
        $image = new Image();
        $image->url = $url;
        $image->advert_id = $advertId;
        $image->thumbnail = env('BASE_CLOUDFRONT_URL') . $fileThumbName;
        $image->save();
        return Response::json($image->id)->header('Content-Type', 'application/json');
    }

    public function removeImage($id)  {
        Image::where('id', $id)->delete();
        return Response::json($id)->header('Content-Type', 'application/json');
    }

    public function removeAdvert(Request $request, $id) {
        Advert::where('id', $id)->delete();
    }

    public function fileUpload($file, $fileName, $thumbnailName) {
        $file->storeAs('images', $fileName);
        $image = \Intervention\Image\Facades\Image::make(storage_path('app') . '/images/' . $fileName)->resize(210, 117);
        $image->save('../storage/app/public/' . $fileName, 85);
        Storage::disk('s3')->put($thumbnailName, file_get_contents(storage_path('app') . '/public/' . $fileName));
        $file->storeAs('', $fileName, 's3');
        Storage::disk('public')->delete($fileName);
        Storage::disk('images')->delete($fileName);
    }

}
