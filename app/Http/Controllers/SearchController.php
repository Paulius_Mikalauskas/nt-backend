<?php

namespace App\Http\Controllers;

use App\Advert;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    public function autoComplete($id)
    {
        return json_decode(file_get_contents('https://maps.googleapis.com/maps/api/place/autocomplete/json?' . $id . '&key=' . env('AUTOCOMPLETE_KEY')), true);
    }

    public function searchValidation(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'nullable|numeric',
            'type' => 'nullable|string|max:30',
            'cityName' => 'nullable|string|max:50',
            'street' => 'nullable|string|max:50',
            'startPrice' => 'numeric|nullable',
            'endPrice' => 'numeric|nullable',
            'roomStartCount' => 'numeric|nullable',
            'roomEndCount' => 'numeric|nullable',
        ]);

        if ($validator->fails())
        {
            abort(500);
        }
    }

    public function search(Request $request)
    {
        $this->searchValidation($request);
        $street = $request->street;
        $words = array("g.", " Gatve", " Gatvė", ' gatve', ' gatvė');
        $str = str_replace($words, "", $street);

        return Advert::with('images')
            ->where(function ($query) use ($request, $str) {
                $query
                    ->where('city', 'like', "%{$request->cityName}%")
                    ->where('type', 'like', "%{$request->type}%")
                    ->when($request->endPrice, function ($query) use ($request) {
                        $query->where('price', '<=', $request->endPrice);
                    })
                    ->when($request->startPrice, function ($query) use ($request) {
                        $query->where('price', '>=', $request->startPrice);
                    })
                    ->when($request->roomEndCount, function ($query) use ($request) {
                        $query->where('rooms', '<=', $request->roomEndCount);
                    })
                    ->when($request->roomStartCount, function ($query) use ($request) {
                        $query->where('rooms', '>=', $request->roomStartCount);
                    })
                    ->when($request->id, function ($query) use ($request) {
                        $query->where('id', $request->id);
                    })
                    ->when($str, function ($query) use ($str) {
                        $query->where('street', 'like', "%{$str}%");
                    });

            })->get();
    }
}