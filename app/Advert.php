<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Advert
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Field[] $fields
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Image[] $images
 * @mixin \Eloquent
 */
class Advert extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

     public function images() {
         return $this->hasMany('App\Image');
     }
}
