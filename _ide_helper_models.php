<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Advert
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Field[] $fields
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Image[] $images
 */
	class Advert extends \Eloquent {}
}

namespace App{
/**
 * App\Field
 *
 */
	class Field extends \Eloquent {}
}

namespace App{
/**
 * App\Image
 *
 */
	class Image extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 */
	class User extends \Eloquent {}
}

