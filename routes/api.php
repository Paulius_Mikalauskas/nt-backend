<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('objects', function () {
    $objects = App\Advert::with('images')->get();
    return Response::json($objects)->header('Content-Type', 'application/json');
});

Route::get('details/{id}', function($objectId) {
    $object =  App\Advert::with('images')->find($objectId);
    return Response::json($object)->header('Content-Type', 'application/json');
});

Route::get('autocomplete/{id}', 'SearchController@autoComplete');

Route::post('search', 'SearchController@search');

Route::post('login', 'AuthController@login');

Route::post('register', 'AuthController@register');

Route::group(['middleware' => 'auth'], function() {

    Route::post('update/{id}', 'AdvertController@update');

    Route::post('addImage/{id}', 'AdvertController@addImage');

    Route::post('removeImage/{id}', 'AdvertController@removeImage');

    Route::post('remove/{id}', 'AdvertController@removeAdvert');

    Route::post('upload', 'AdvertController@insert');
});
